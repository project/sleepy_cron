<?php

namespace Drupal\sleepy_cron;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\State\StateInterface;
use Drupal\Core\File\FileExists;

/**
 * This stores the cron's "state" (awake or asleep) and the latest time at which
 * the website was used.
 *
 * State is stored in a file so that it can easily be checked by an external
 * script. This allows this external script to not even execute the
 * cron-triggering command if it's asleep, thus preventing a Drush/Drupal
 * bootstrap, for better performance.
 * Anyway, if the cron-triggering command is executed while cron is asleep, it
 * will do nothing but a bootstrap and a check. No jobs or queues execution.
 */
class SleepyCronStatus {

  const FILE_NAME = 'cron_is_asleep.txt';

  const FILE_CONTENT = "This file created by the Sleepy cron Drupal module
stores the fact that the cron system is currently asleep because the website
has not been used for a long time. It's checked by the module to prevent cron
execution, but can be checked by an external script even before triggering a
cron execution, for better performance.";

  const STATE_NAME = 'sleepy_cron_latest_usage';

  /**
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private FileSystemInterface $fileSystem;

  /**
   * @var \Drupal\Core\State\StateInterface
   */
  private StateInterface $state;

  public function __construct(FileSystemInterface $file_system, StateInterface $state) {
    $this->fileSystem = $file_system;
    $this->state = $state;
  }

  public function cronIsAsleep(): bool {
    $mask = '/^' . preg_quote(self::FILE_NAME) . '$/';
    return !empty($this->fileSystem->scanDirectory($this->getCronStateFileDir(), $mask, ['recurse' => FALSE]));
  }

  public function getCronToSleep(): void {
    $this->fileSystem->saveData(self::FILE_CONTENT, $this->getCronStateFileUri(), FileExists::Replace);
  }

  public function wakeCronUp(): void {
    $this->fileSystem->unlink($this->getCronStateFileUri());
  }

  public function getLatestRecordedUsageTime(): int {
    return $this->state->get(self::STATE_NAME, 0);
  }

  public function recordUsageTime(int $usage_time): void {
    $this->state->set(self::STATE_NAME, $usage_time);
  }

  /**
   * Allows to easily disable the feature on some environments such as prod.
   * @return bool
   */
  public function sleepyCronFeatureIsEnabled() : bool {
    return Settings::get('sleepy_cron.enabled', TRUE);
  }

  private function getCronStateFileUri(): string {
    return $this->getCronStateFileDir() . '/' . self::FILE_NAME;
  }

  private function getCronStateFileDir(): string {
    /*
     * We default to the public dir because:
     * - it's writable,
     * - a private dir might not exist,
     * - it's not secret data,
     * - and there's a different one for each different site in multisite mode.
     */
    return Settings::get('sleepy_cron.cron_state_file_dir', 'public://');
  }

}
