<?php

namespace Drupal\sleepy_cron;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\CronInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 *
 */
class SleepyCronEventSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * @var \Drupal\sleepy_cron\SleepyCronStatus
   */
  private SleepyCronStatus $status;

  /**
   * @var \Drupal\Core\CronInterface
   */
  private CronInterface $cron;

  /**
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  private AccountProxyInterface $currentUser;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  private MessengerInterface $messenger;

  /**
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  private RouteMatchInterface $currentRouteMatch;

  /**
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  private TimeInterface $time;

  public function __construct(
    SleepyCronStatus $status,
    CronInterface $cron,
    AccountProxyInterface $current_user,
    MessengerInterface $messenger,
    RouteMatchInterface $current_route_match,
    TimeInterface $time
  ) {
    $this->status = $status;
    $this->cron = $cron;
    $this->currentUser = $current_user;
    $this->messenger = $messenger;
    $this->currentRouteMatch = $current_route_match;
    $this->time = $time;
  }

  public function wakeCronUpIfAsleep(): void {
    if (!$this->status->sleepyCronFeatureIsEnabled()) {
      return;
    }

    if ($this->status->cronIsAsleep()) {
      // If cron is launched through the cron launch URL, we do not want to wake
      // cron up. This is not a "real usage" of the website.
      if (!$this->requestIsRealUsage()) {
        return;
      }

      // This is the first request for a long time.
      // The website is used again, so crons should run normally.
      $this->status->wakeCronUp();

      // Some cron jobs should have run but could not because cron was asleep.
      // So we execute cron once now. If there are queues, they may not have run
      // entirely but that's way better than nothing.
      $this->cron->run();

      // We tell about the risk of late crons.
      if ($this->currentUser->hasPermission('see cron wakeup warning')) {
        $this->messenger->addWarning($this->t('You are the first to
 request this website for a long time. So that to preserve server
 resources, cron had been temporarily disabled. It just ran again, but you
 may expect strange behaviors because some queues handled on cron (such as
 content indexation for search) could still have to be fully processed.'));
      }
    }
  }

  public function keepCronAwake(): void {
    if (!$this->status->sleepyCronFeatureIsEnabled()) {
      return;
    }

    // If cron is launched through the cron launch URL, we do not want to keep
    // cron awake. This is not a "real usage" of the website.
    if (!$this->requestIsRealUsage()) {
      return;
    }

    // We do not record every request for performance reasons.
    // Using session write interval seems like a good trade-off.
    $now = $this->time->getRequestTime();
    if ($this->status->getLatestRecordedUsageTime() < $now - Settings::get('session_write_interval', 180)) {
      $this->status->recordUsageTime($now);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['wakeCronUpIfAsleep'];
    $events[KernelEvents::TERMINATE][] = ['keepCronAwake'];
    return $events;
  }

  private function requestIsRealUsage(): bool {
    return !in_array($this->currentRouteMatch->getRouteName(), [
      // The route to launch the cron from outside:
      'system.cron',
      // The admin button to launch the cron send a POST request to:
      'system.cron_settings',
      // No route name, it's probably a drush call.
      NULL,
    ]);
  }

}
